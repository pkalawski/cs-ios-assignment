
# Implementation
1. ImageCaching: I've used Kingfisher caching algorithm.
2. RatingView - Building with 3 layers to enable simple animation and separate each part of RatingView.

# SPM

I've divided application code into 2 SPM and main app code. 

1. Networking SPM - responsible for networking in application. We can easily replace it with a different networking implementation.
2. DAL(Data Access Layer) SPM - deliver models which can be use in main application code.


# 3'rd Libraries
1. SnapKit - enable writing elegance constrants code. In main project we can write own code to handle constraints and don't use "syntactic sugar" delivered by SnapKit.
2. Kingfisher - great implemmentation of caching images.