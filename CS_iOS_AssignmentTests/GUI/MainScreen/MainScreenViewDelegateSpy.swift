//
//  MainScreenViewDelegateSpy.swift
//  CS_iOS_AssignmentTests
//
//  Created by Przemysław Kalawski on 03/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import UIKit
@testable import CS_iOS_Assignment

final class MainScreenViewDelegateSpy: MainScreenViewDelegate {
    
    private(set) var presentDetailsViewInvoked: [UIViewController] = []
    private(set) var didReloadCollectionViewInvoked = false
    private(set) var reloadCollectionViewInvokedCount = 0
    private(set) var didReloadTableViewInvoked = false
    private(set) var reloadTableViewInvokedCount = 0
    private(set) var didPresentAlertControllerInvoked = false
    private(set) var presentAlertControllerInvoked: [UIViewController] = []
    
    func presentDetailsView(_ viewController: UIViewController) {
        self.presentDetailsViewInvoked.append(viewController)
    }
    
    func reloadCollectionView() {
        self.reloadCollectionViewInvokedCount += 1
        self.didReloadCollectionViewInvoked = true
    }
    
    func reloadTableView() {
        self.reloadTableViewInvokedCount += 1
        self.didReloadTableViewInvoked = true
    }
    
    func presentAlertController(_ viewController: UIViewController) {
        self.didPresentAlertControllerInvoked = true
        self.presentAlertControllerInvoked.append(viewController)
    }
}
