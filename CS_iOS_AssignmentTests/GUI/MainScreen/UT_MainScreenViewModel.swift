//
//  UT_MainScreenViewModel.swift
//  CS_iOS_AssignmentTests
//
//  Created by Przemysław Kalawski on 03/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import XCTest
import DAL
import Networking

@testable import CS_iOS_Assignment

final class UT_MainScreenViewModel: XCTestCase {
    
    private let networkingMock = NetworkingMock()
    private lazy var dal = DAL(networking: networkingMock)
    private let imageRepository = ImageRepositoryStub()
    private let delegateSpy = MainScreenViewDelegateSpy()
    private lazy var sut = MainScreenViewModel(dal: dal, imageRepository: imageRepository)
    
    override func setUp() {
        super.setUp()
        sut.delegate = delegateSpy
        imageRepository.getImageFromStub = { url, completion in
            completion(.success(UIImage()))
        }
    }
    
    func testFetchingPopularMovies() {
        networkingMock
            .when(PopularMoviesRequest.self)
            .then { request -> Result<PopularMoviesResponse, NetworkingError> in
                .success(PopularMoviesResponse.popularMovies)
            }
        
        sut.fetchData()
        
        let predicate = NSPredicate { [delegateSpy] _, _ in
            delegateSpy.didReloadTableViewInvoked
        }

        let reloadTableViewExpectation = expectation(for: predicate, evaluatedWith: delegateSpy, handler: .none)
        wait(for: [reloadTableViewExpectation], timeout: 3.0)
        
        XCTAssertEqual(delegateSpy.reloadTableViewInvokedCount, 1, "TableView with popular movies should be reload!")
    }
    
    func testFetchingPlayingNowMovies() {
        networkingMock
            .when(PlayingNowRequest.self)
            .then { request -> Result<PlayingNowResponse, NetworkingError> in
                .success(PlayingNowResponse.playingNowResponse)
            }
        
        sut.fetchData()
        
        let predicate = NSPredicate { [delegateSpy] _, _ in
            delegateSpy.didReloadCollectionViewInvoked
        }

        let reloadCollectionViewExpectation = expectation(for: predicate, evaluatedWith: delegateSpy, handler: .none)
        wait(for: [reloadCollectionViewExpectation], timeout: 3.0)
        
        XCTAssertEqual(delegateSpy.reloadCollectionViewInvokedCount, 1, "CollectionView with popular movies should be reload!")
    }
    
    func testFetchingPlayingNowMoviesWithFailure() {
        networkingMock
            .when(PlayingNowRequest.self)
            .then { request -> Result<PlayingNowResponse, NetworkingError> in
                .failure(NetworkingError.unknown(error: NSError(domain: "", code: 1, userInfo: nil)))
            }
        
        sut.fetchData()
        
        let predicate = NSPredicate { [delegateSpy] _, _ in
            delegateSpy.didPresentAlertControllerInvoked
        }

        let reloadCollectionViewExpectation = expectation(for: predicate, evaluatedWith: delegateSpy, handler: .none)
        wait(for: [reloadCollectionViewExpectation], timeout: 3.0)
        
        XCTAssertEqual(delegateSpy.presentAlertControllerInvoked.count, 1, "AlertView should be presented!")
    }
    
    func testFetchingPopularMoviesWithFailure() {
        networkingMock
            .when(PopularMoviesRequest.self)
            .then { request -> Result<PopularMoviesResponse, NetworkingError> in
                .failure(NetworkingError.unknown(error: NSError(domain: "", code: 1, userInfo: nil)))
            }
        
        sut.fetchData()
        
        let predicate = NSPredicate { [delegateSpy] _, _ in
            delegateSpy.didPresentAlertControllerInvoked
        }

        let reloadTableViewExpectation = expectation(for: predicate, evaluatedWith: delegateSpy, handler: .none)
        wait(for: [reloadTableViewExpectation], timeout: 3.0)
        
        XCTAssertEqual(delegateSpy.presentAlertControllerInvoked.count, 1, "AlertView should be presented!")
    }
}
