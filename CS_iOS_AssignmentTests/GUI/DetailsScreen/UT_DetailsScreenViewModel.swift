//
//  UT_DetailsScreenViewModel.swift
//  CS_iOS_AssignmentTests
//
//  Created by Przemysław Kalawski on 03/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import XCTest
import DAL
import UIKit
import Networking

@testable import CS_iOS_Assignment

final class UT_DetailsScreenViewModel: XCTestCase {
    
    private let networkingMock = NetworkingMock()
    private lazy var dal = DAL(networking: networkingMock)
    private let imageRepository = ImageRepositoryStub()
    private let delegateSpy = DetailsScreenViewDelegateSpy()
    private let movie = Movie.noGoodDeadMovie
    private let timeConverter = TimeConverter()
    private let dateConverter = DateConverter()
    private lazy var sut = DetailsScreenViewModel(movie: movie, timeConverter: timeConverter, dateConverter: dateConverter, dal: dal, imageRepository: imageRepository)
    
    override func setUp() {
        super.setUp()
        sut.delegate = delegateSpy
        imageRepository.getImageFromStub = { url, completion in
            completion(.success(UIImage()))
        }
    }
    
    func testMovieTitle() {
        sut.viewDidLoad()
        XCTAssertEqual(delegateSpy.updateMovieTitleWithInvoked.first, movie.title, "Title label text should be the same as title from movie model!")
    }
    
    func testOverview() {
        sut.viewDidLoad()
        XCTAssertEqual(delegateSpy.updateOverviewWithInvoked.first, movie.overview, "Overview text should be the same as overview from movie model!")
    }
    
    func testImageUpdateWithError() {
        imageRepository.getImageFromStub = { url, completion in
            completion(.failure(NSError(domain: "", code: 10, userInfo: nil)))
        }
        
        sut.viewDidLoad()
        let predicate = NSPredicate { [delegateSpy] _, _ in
            delegateSpy.didUpdatePoster
        }
        let imageFetchingExpectation = expectation(for: predicate, evaluatedWith: delegateSpy, handler: .none)
        wait(for: [imageFetchingExpectation], timeout: 2.0)
        
        XCTAssertEqual(delegateSpy.updatePosterWithInvoked.first, UIImage(named: "placeholder"), "Placeholder image has not been set!")
    }
    
    func testImageUpdateWithSuccess() {
        let image = UIImage()
        imageRepository.getImageFromStub = { url, completion in
            completion(.success(image))
        }
        
        sut.viewDidLoad()
        let predicate = NSPredicate { [delegateSpy] _, _ in
            delegateSpy.didUpdatePoster
        }
        let imageFetchingExpectation = expectation(for: predicate, evaluatedWith: delegateSpy, handler: .none)
        wait(for: [imageFetchingExpectation], timeout: 2.0)
        
        XCTAssertEqual(delegateSpy.updatePosterWithInvoked.first, image, "Image has not been set!")
    }
    
    func testMovieDescriptionUpdate() {
        let movieDetails = MovieDetails.noGoodDeadMovie
        let runtime = timeConverter.toHours(runtime: movieDetails.runtime)
        let releaseDate = dateConverter.convertDate(from: movieDetails.releaseDate)!
        
        networkingMock
            .when(MovieDetailsRequest.self)
            .then { request -> Result<NetworkingModel.MovieDetailsDTO, NetworkingError> in
                .success(NetworkingModel.MovieDetailsDTO.noGoodDeadMovieDetailsDTO)
            }
        
        sut.viewDidLoad()
        
        let predicate = NSPredicate { [delegateSpy] _, _ in
            delegateSpy.didUpdateDescription
        }
        let gettingDescriptionExpectation = expectation(for: predicate, evaluatedWith: delegateSpy, handler: .none)
        wait(for: [gettingDescriptionExpectation], timeout: 2.0)
        
        XCTAssertEqual(delegateSpy.updateDescriptionWithInvoked.first,"\(releaseDate) - \(runtime.hours)h \(runtime.minutes)m" , "Description has bad format!")
    }
    
    func testGenresUpdate() {
        networkingMock
            .when(MovieDetailsRequest.self)
            .then { request -> Result<NetworkingModel.MovieDetailsDTO, NetworkingError> in
                .success(NetworkingModel.MovieDetailsDTO.noGoodDeadMovieDetailsDTO)
            }
        
        sut.viewDidLoad()
        
        let predicate = NSPredicate { [delegateSpy] _, _ in
            delegateSpy.didUpdateGenres
        }
        let gettingGenresExpectation = expectation(for: predicate, evaluatedWith: delegateSpy, handler: .none)
        wait(for: [gettingGenresExpectation], timeout: 3.0)
        
        XCTAssertEqual(delegateSpy.updateGenresInvokedCount, 1 , "Genres should be updated!")
    }
}
