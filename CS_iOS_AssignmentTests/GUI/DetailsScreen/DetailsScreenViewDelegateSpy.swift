//
//  DetailsScreenViewDelegateSpy.swift
//  CS_iOS_AssignmentTests
//
//  Created by Przemysław Kalawski on 03/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import UIKit

@testable import CS_iOS_Assignment

final class DetailsScreenViewDelegateSpy: DetailsScreenViewDelegate {
    
    private(set) var updateMovieTitleWithInvoked: [String] = []
    private(set) var didUpdateDescription = false
    private(set) var updateDescriptionWithInvoked: [String?] = []
    private(set) var updateOverviewWithInvoked: [String] = []
    private(set) var didUpdatePoster = false
    private(set) var updatePosterWithInvoked: [UIImage?] = []
    private(set) var didUpdateGenres = false
    private(set) var updateGenresInvokedCount: Int = 0
    
    func updateMovieTitleWith(_ title: String) {
        self.updateMovieTitleWithInvoked.append(title)
    }
    
    func updateDescriptionWith(_ dateString: String?) {
        self.updateDescriptionWithInvoked.append(dateString)
        self.didUpdateDescription = true
    }
    
    func updateOverviewWith(_ overview: String) {
        self.updateOverviewWithInvoked.append(overview)
    }
    
    func updatePosterWith(_ image: UIImage?) {
        self.updatePosterWithInvoked.append(image)
        self.didUpdatePoster = true
    }
    
    func updateGenres() {
        self.updateGenresInvokedCount += 1
        self.didUpdateGenres = true
    }
}
