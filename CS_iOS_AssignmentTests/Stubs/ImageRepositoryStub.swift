//
//  ImageRepositoryStub.swift
//  CS_iOS_AssignmentTests
//
//  Created by Przemysław Kalawski on 03/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import UIKit
@testable import CS_iOS_Assignment

final class ImageRepositoryStub: ImageRepositoryProtocol, Stubbable {
    
    public lazy var getImageFromStub = stub(of: getImageFrom(url:completion:))
    
    func getImageFrom(url: String, completion: @escaping (Result<UIImage, Error>) -> Void) {
        getImageFromStub(url, completion)
    }
}
