//
//  Stubbable.swift
//  CS_iOS_AssignmentTests
//
//  Created by Przemysław Kalawski on 03/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

protocol Stubbable {
    func stub<Type>(of type: Type) -> Type
}

extension Stubbable {
    func stub<Type>(of type: Type) -> Type {
        fatalError()
    }
}
