//
//  MovieFakes.swift
//  CS_iOS_AssignmentTests
//
//  Created by Przemysław Kalawski on 03/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Networking
import DAL

extension MovieDTO {
    static let noGoodDeadMovieDTO = MovieDTO(adult: true,
                                        backdropPath: "/lrzAb9QCwKPOwH1iY7WpfjR6jcC.jpg",
                                        genreIds: [10770, 53],
                                        id: 648371,
                                        originalLanguage: "en",
                                        originalTitle: "No Good Deed",
                                        overview: "Karen never planned on being a hero. A recent widow, she has her hands full with work and parenting her son Max. Then Karen saves Jeremy\'s life during a drug store robbery and quickly",
                                        popularity: 1037.871,
                                        posterPath: "/27BnREBR5rcMrUVUhV5bTXlcCym.jpg",
                                        releaseDate: "2020-03-13",
                                        title: "No Good Deed",
                                        video: false,
                                        voteAverage: 6.4,
                                        voteCount: 20)
}

extension Movie {
    static let noGoodDeadMovie = Movie(.noGoodDeadMovieDTO)
}

extension NetworkingModel.MovieDetailsDTO {
    static let noGoodDeadMovieDetailsDTO = NetworkingModel.MovieDetailsDTO(adult: true,
                                                                 backdropPath: "/lrzAb9QCwKPOwH1iY7WpfjR6jcC.jpg",
                                                                 budget: 123123123,
                                                                 genres: [],
                                                                 homepage: "homepage.com",
                                                                 id: 648371,
                                                                 imdbId: "648371",
                                                                 originalLanguage: "en",
                                                                 originalTitle: "No Good Deed",
                                                                 overview: "Karen never planned on being a hero. A recent widow, she has her hands full with work and parenting her son Max. Then Karen saves Jeremy\'s life during a drug store robbery and quickly",
                                                                 popularity: 1037.871,
                                                                 posterPath: "path",
                                                                 productionCompanies: [],
                                                                 productionCountries: [],
                                                                 releaseDate: "2020-03-13",
                                                                 revenue: 234235345,
                                                                 runtime: 90,
                                                                 spokenLanguages: [],
                                                                 status: "",
                                                                 tagline: "",
                                                                 title: "No Good Deed",
                                                                 video: false,
                                                                 voteAverage: 6.4,
                                                                 voteCount: 20)
}

extension MovieDetails {
    static let noGoodDeadMovie = MovieDetails(.noGoodDeadMovieDetailsDTO)
}
