//
//  PlayingNowResponse.swift
//  CS_iOS_AssignmentTests
//
//  Created by Przemysław Kalawski on 03/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import Networking

extension PlayingNowResponse {
    static let playingNowResponse = PlayingNowResponse(dates: Dates(maximum: "maximum", minimum: "minimum"),
                                                        page: 1,
                                                        results: [.noGoodDeadMovieDTO, .noGoodDeadMovieDTO],
                                                        totalPages: 10,
                                                        totalResults: 100)
}
