//
//  ImageRepository.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit
import Kingfisher

protocol ImageRepositoryProtocol {
    func getImageFrom(url: String, completion: @escaping (Result<UIImage, Error>) -> Void)
}

final class ImageRepository: ImageRepositoryProtocol {
    
    private let kingfisherManager: KingfisherManager
    
    init(kingfisherManager: KingfisherManager = .shared) {
        self.kingfisherManager = kingfisherManager
    }
    
    func getImageFrom(url: String, completion: @escaping (Result<UIImage, Error>) -> Void) {
        guard let url = URL(string: url) else {
            completion(.failure(ImageRepositoryError.failedURL))
            return
        }
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.kingfisherManager.retrieveImage(with: url,
                                                  options: [.backgroundDecode,
                                                            .cacheOriginalImage]) { result in
                switch result {
                case let .success(data):
                    completion(.success(data.image))
                case let .failure(error):
                    completion(.failure(error))
                }
            }
        }
    }
}
