//
//  ImageRepositoryError.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

enum ImageRepositoryError: Error {
    case failedURL
}
