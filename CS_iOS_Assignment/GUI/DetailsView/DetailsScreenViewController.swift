//
//  DetailsScreenViewController.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class DetailsScreenViewController: UIViewController {
    
    enum Const {
        static let closeImage = UIImage(named: "icon_close")
        static let labelTextColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0)
        static let titleFont = UIFont(name: "HelveticaNeue-Bold", size: 16.0)
        static let contentFont = UIFont(name: "HelveticaNeue", size: 14.0)
        static let overviewHeaderText = "Overview"
        
        enum PosterImage {
            static let borderWidth: CGFloat = 2.0
            static let borderColor = UIColor(red: 130/255, green: 130/255, blue: 130/255, alpha: 1.0).cgColor
            static let imageViewSize = CGSize(width: 135.0, height: 200.0)
        }
        
        enum Genres {
            static let lineSpacing: CGFloat = 8.0
            static let interitemSpacing: CGFloat = 8.0
        }
    }
    
    private lazy var contentScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.contentInsetAdjustmentBehavior = .never
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        let image = Const.closeImage?.withTintColor(Const.labelTextColor)
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        return button
    }()

    private lazy var posterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.borderWidth = Const.PosterImage.borderWidth
        imageView.layer.borderColor = Const.PosterImage.borderColor
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        var label = UILabel()
        label.font = Const.titleFont
        label.textColor = Const.labelTextColor
        label.numberOfLines = 2
        label.textAlignment = .center
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        var label = UILabel()
        label.font = Const.contentFont
        label.textColor = Const.labelTextColor
        label.textAlignment = .center
        return label
    }()
    
    private lazy var overviewTitleLabel: UILabel = {
        var label = UILabel()
        label.font = Const.titleFont
        label.textColor = Const.labelTextColor
        label.text = Const.overviewHeaderText
        return label
    }()
    
    private lazy var overviewLabel: UILabel = {
        var label = UILabel()
        label.font = Const.contentFont
        label.textColor = Const.labelTextColor
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var genresCollectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = Const.Genres.lineSpacing
        flowLayout.minimumInteritemSpacing = Const.Genres.interitemSpacing
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.register(GenreCollectionViewCell.self, forCellWithReuseIdentifier: GenreCollectionViewCell.identifier)
        collectionView.allowsSelection = false
        collectionView.dataSource = viewModel.genresCollectionDataManager
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()
    
    private let viewModel: DetailsScreenViewModel
    
    init(viewModel: DetailsScreenViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        setupConstraints()
        viewModel.viewDidLoad()
    }
}

extension DetailsScreenViewController {
    private func setupLayout() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.82)
        view.addSubview(contentScrollView)
        contentScrollView.addSubview(contentView)
        contentView.addSubview(closeButton)
        contentView.addSubview(posterImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(overviewTitleLabel)
        contentView.addSubview(overviewLabel)
        contentView.addSubview(genresCollectionView)
    }
    
    private func setupConstraints() {
        contentScrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.width.equalTo(view.snp.width)
        }
        
        closeButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(35.0)
            make.trailing.equalToSuperview().offset(-24.0)
            make.size.equalTo(18.0)
        }
        
        posterImageView.snp.makeConstraints { make in
            make.top.equalTo(closeButton.snp.bottom).offset(26.0)
            make.centerX.equalToSuperview()
            make.size.equalTo(Const.PosterImage.imageViewSize)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(posterImageView.snp.bottom).offset(8.0)
            make.centerX.equalToSuperview()
            make.leading.equalToSuperview().offset(8.0)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(4.0)
            make.centerX.equalToSuperview()
            make.leading.equalToSuperview().offset(8.0)
        }
        
        overviewTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(descriptionLabel.snp.bottom).offset(20.0)
            make.leading.trailing.equalToSuperview().inset(40.0)
        }
        
        overviewLabel.snp.makeConstraints { make in
            make.top.equalTo(overviewTitleLabel.snp.bottom).offset(8.0)
            make.leading.trailing.equalToSuperview().inset(40.0)
        }
        
        genresCollectionView.snp.makeConstraints { make in
            make.top.equalTo(overviewLabel.snp.bottom).offset(18.0)
            make.leading.trailing.equalToSuperview().inset(40.0)
            make.bottom.equalToSuperview().offset(-44.0)
            make.height.equalTo(30.0)
        }
    }
}

extension DetailsScreenViewController {
    @objc private func dismissView() {
        dismiss(animated: true, completion: nil)
    }
}

extension DetailsScreenViewController: DetailsScreenViewDelegate {
    func updateDescriptionWith(_ dateString: String?) {
        descriptionLabel.text = dateString
    }
    
    func updateGenres() {
        genresCollectionView.reloadData()
    }
    
    func updateMovieTitleWith(_ title: String) {
        titleLabel.text = title
    }
    
    func updateOverviewWith(_ overview: String) {
        overviewLabel.text = overview
    }
    
    func updatePosterWith(_ image: UIImage?) {
        posterImageView.image = image
    }
}
