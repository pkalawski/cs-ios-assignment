//
//  GenresCollectionDataManager.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import UIKit
import DAL

final class GenresCollectionDataManager: NSObject {
    
    var items: [Genre] = []
}

extension GenresCollectionDataManager: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: GenreCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath) else { return UICollectionViewCell() }
        cell.setUpTitle(items[indexPath.row].name)
        return cell
    }
}
