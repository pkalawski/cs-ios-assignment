//
//  GenreCollectionViewCell.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import UIKit

final class GenreCollectionViewCell: UICollectionViewCell {
    
    enum Const {
        static let titleTextColor = UIColor.black
        static let titleFont = UIFont(name: "HelveticaNeue", size: 14.0)
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Const.titleFont
        label.textColor = Const.titleTextColor
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension GenreCollectionViewCell {
    func setUpTitle(_ title: String) {
        titleLabel.text = title.uppercased()
    }
}

extension GenreCollectionViewCell {
    private func setupLayout() {
        backgroundColor = .white
        layer.cornerRadius = 4.0
        contentView.addSubview(titleLabel)
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(4.0)
            make.leading.trailing.equalToSuperview().inset(8.0)
        }
    }
}
