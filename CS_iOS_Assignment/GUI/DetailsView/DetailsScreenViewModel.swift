//
//  DetailsScreenViewModel.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import UIKit
import Combine
import DAL
import Networking

protocol DetailsScreenViewDelegate: AnyObject {
    func updateMovieTitleWith(_ title: String)
    func updateDescriptionWith(_ dateString: String?)
    func updateOverviewWith(_ overview: String)
    func updatePosterWith(_ image: UIImage?)
    func updateGenres()
}

final class DetailsScreenViewModel {
    
    weak var delegate: DetailsScreenViewDelegate?
    
    private let movie: Movie
    private let timeConverter: TimeConverter
    private let dateConverter: DateConverter
    private let dal: DAL
    private let imageRepository: ImageRepositoryProtocol
    
    private(set) var genresCollectionDataManager = GenresCollectionDataManager()
    
    private var disposables = Set<AnyCancellable>()
    
    init(movie: Movie,
         timeConverter: TimeConverter = TimeConverter(),
         dateConverter: DateConverter = DateConverter(),
         dal: DAL = DAL.shared,
         imageRepository: ImageRepositoryProtocol = ImageRepository()) {
        self.movie = movie
        self.timeConverter = timeConverter
        self.dateConverter = dateConverter
        self.dal = dal
        self.imageRepository = imageRepository
    }
}

extension DetailsScreenViewModel {
    func viewDidLoad() {
        updateTitle()
        updateOverview()
        fetchImage()
        fetchGenres()
    }
    
    private func updateTitle() {
        delegate?.updateMovieTitleWith(movie.title)
    }
    
    private func updateOverview() {
        delegate?.updateOverviewWith(movie.overview)
    }
    
    private func fetchImage() {
        if let posterPath = movie.posterPath {
            imageRepository.getImageFrom(url: "\(URLConst.imageUrlStringBaseURL)\(posterPath)", completion: { [delegate] result in
                switch result {
                case let .success(image):
                    DispatchQueue.main.async {
                        delegate?.updatePosterWith(image)
                    }
                case .failure(_):
                    delegate?.updatePosterWith(UIImage(named: "placeholder"))
                }
            })
        } else {
            delegate?.updatePosterWith(UIImage(named: "placeholder"))
        }
    }
    
    private func fetchGenres() {
        dal.executeRequest(MovieDetailsRequest(movieId: movie.id))?
            .receive(on: DispatchQueue.main)
            .sink { response in
                print(response)
            } receiveValue: { [weak self] movieDetails in
                guard let self = self else { return }
                self.delegate?.updateDescriptionWith(self.prepareDescription(with: movieDetails))
                self.genresCollectionDataManager.items = movieDetails.genres
                self.delegate?.updateGenres()
            }.store(in: &disposables)
    }
    
    private func prepareDescription(with movieDetails: MovieDetails) -> String {
        var descriptions = ""
        if let releaseDate = dateConverter.convertDate(from: movieDetails.releaseDate) {
            descriptions = releaseDate
        }
        let runtime = self.timeConverter.toHours(runtime: movieDetails.runtime)
        descriptions += " - \(runtime.hours)h \(runtime.minutes)m"
        return descriptions
    }
}
