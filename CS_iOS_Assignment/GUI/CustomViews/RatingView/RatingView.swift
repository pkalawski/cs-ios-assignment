//
//  RatingView.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

final class RatingView: UIView {
    
    enum Const {
        static let lineWidth: CGFloat = 4.0
        static let highRatingColor = UIColor(red: 37/255, green: 201/255, blue: 113/255, alpha: 1.0)
        static let lessRatingColor = UIColor(red: 203/255, green: 208/255, blue: 56/255, alpha: 1.0)
        
        static let circleLineBackgroundColor = UIColor(red: 30/255, green: 60/255, blue: 36/255, alpha: 1.0).cgColor
        static let backgroundColor = UIColor(red: 11/255, green: 25/255, blue: 31/255, alpha: 1.0).cgColor
        
        static let contentFont = UIFont(name: "HelveticaNeue-Bold", size: 14.0)
        static let smallContentFont = UIFont(name: "HelveticaNeue-Bold", size: 5.0)
    }
    
    private let circleLayer = CAShapeLayer()
    private let ovalLayer = CAShapeLayer()
    private let progressLayer = CAShapeLayer()
    private let ratingAnimation = CABasicAnimation(keyPath: "strokeEnd")
    
    private var progressColor = Const.highRatingColor
    
    var progress: CGFloat = 0.0 {
        didSet {
            updateRatingLabel()
            progressColor = progress >= 0.5 ? Const.highRatingColor : Const.lessRatingColor
            updateLayers(with: frame)
            setupAnimation()
            startAnimation()
        }
    }
    
    private lazy var ratingLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = Const.contentFont
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayers()
        setupSubviews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension RatingView {
    private func setupLayers() {
        ovalLayer.lineWidth = 6.0
        ovalLayer.strokeColor = Const.backgroundColor
        layer.addSublayer(ovalLayer)
        
        circleLayer.lineWidth = Const.lineWidth
        circleLayer.fillColor = Const.backgroundColor
        layer.addSublayer(circleLayer)
        
        progressLayer.lineWidth = Const.lineWidth
        progressLayer.fillColor = nil
        layer.addSublayer(progressLayer)
    }
    
    private func setupSubviews() {
        addSubview(ratingLabel)
        ratingLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}

extension RatingView {
    private func updateLayers(with frame: CGRect) {
        let circlePath = UIBezierPath(ovalIn: frame).cgPath
        ovalLayer.path = circlePath
        
        let centerPoint = CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0)

        let inside = UIBezierPath(arcCenter: centerPoint, radius: (frame.size.height / 2.0) - 2.0, startAngle: -0.5 * .pi, endAngle: 1.5 * .pi, clockwise: true).cgPath
        circleLayer.path = inside
        circleLayer.strokeStart = progress
        circleLayer.strokeEnd = 1.0
        circleLayer.strokeColor = Const.circleLineBackgroundColor
        
        progressLayer.path = inside.copy()
        progressLayer.strokeStart = 0
        progressLayer.strokeEnd = progress
        progressLayer.strokeColor = progressColor.cgColor
        progressLayer.lineCap = .round
    }
    
    private func setupAnimation() {
        ratingAnimation.duration = 0.5
        ratingAnimation.fromValue = 0.0
        ratingAnimation.toValue = progress
        ratingAnimation.fillMode = .forwards
        ratingAnimation.isRemovedOnCompletion = true
    }
    
    private func startAnimation() {
        progressLayer.add(ratingAnimation, forKey: "animationsKey")
    }
    
    private func updateRatingLabel() {
        let numberAttributedString = NSMutableAttributedString(string: "\(Int(progress * 100))", attributes: [NSAttributedString.Key.font: Const.contentFont])
        let offset = (Const.contentFont?.capHeight ?? 1.5) / 1.5
        let percentageAttributedString = NSAttributedString(string: "%", attributes: [NSAttributedString.Key.baselineOffset: offset, NSAttributedString.Key.font: Const.smallContentFont])
        numberAttributedString.append(percentageAttributedString)
        ratingLabel.attributedText = numberAttributedString
    }
}
