//
//  SectionHeaderView.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit
import SnapKit

final class SectionHeaderView: UIView {
    
    private lazy var label: UILabel = {
        var label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 12.0)
        label.textColor = UIColor(red: 252/255, green: 208/255, blue: 82/255, alpha: 1.0)
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        setupLayout()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SectionHeaderView {
    func setTitle(_ title: String) {
        label.text = title
    }
}

extension SectionHeaderView {
    private func setupLayout() {
        addSubview(label)
    }
    
    private func setupConstraints() {
        label.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(4.0)
            make.leading.trailing.equalToSuperview().inset(20.0)
        }
    }
}
