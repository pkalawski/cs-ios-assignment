//
//  MainScreenTableDataManager.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit
import Kingfisher
import DAL

final class MainScreenTableDataManager: NSObject {
    
    var items: [MovieCellViewModel] = []
    var imageRepository: ImageRepositoryProtocol?
    var didSelectItemAction: ((Movie) -> Void)?
    var lastItemWasReached: (() -> Void)?
}

extension MainScreenTableDataManager: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: MovieTableViewCell = tableView.dequeueReusableCell(for: indexPath) else {
            fatalError("Cannot dequeue cell of type MovieTableViewCell!")
        }

        let item = items[indexPath.row]
        cell.viewModel = item
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == items.count - 1 {
            lastItemWasReached?()
        }
    }
}

extension MainScreenTableDataManager: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectItemAction?(items[indexPath.row].movie)
    }
}
