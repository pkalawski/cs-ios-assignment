//
//  MainScreenCollectionDataManager.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import UIKit
import DAL

final class MainScreenCollectionDataManager: NSObject {
    
    var items: [MovieCollectionCellModel] = []
    var imageRepository: ImageRepositoryProtocol?
    var didSelectItemAction: ((Movie) -> Void)?
}

extension MainScreenCollectionDataManager: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: MovieCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath) else { return UICollectionViewCell() }
        cell.viewModel = items[indexPath.row]
        return cell
    }
}

extension MainScreenCollectionDataManager: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectItemAction?(items[indexPath.row].movie)
    }
}
