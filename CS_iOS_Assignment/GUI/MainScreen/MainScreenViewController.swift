//
//  MainScreenViewController.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

final class MainScreenViewController: UIViewController {
    
    enum Const {
        static let viewTitle = "MOVIE BOX"
        static let playingNowTitle = "Playing now"
        static let mostPopularTitle = "Most popular"
        
        static let backgroundColor = UIColor(red: 64/255, green: 64/255, blue: 64/255, alpha: 1.0)
    }
    
    private(set) lazy var playingNowHeaderView: SectionHeaderView = {
        let sectionView = SectionHeaderView()
        sectionView.backgroundColor = Const.backgroundColor
        sectionView.setTitle(Const.playingNowTitle)
        return sectionView
    }()
    
    private(set) lazy var playingNowMoviewCollectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0.0
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.estimatedItemSize = MovieCollectionViewCell.Const.imageSize
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: MovieCollectionViewCell.identifier)
        collectionView.dataSource = viewModel.collectionDataSource
        collectionView.delegate = viewModel.collectionDataSource
        return collectionView
    }()
    
    private(set) lazy var popularHeaderView: SectionHeaderView = {
        let sectionView = SectionHeaderView()
        sectionView.backgroundColor = Const.backgroundColor
        sectionView.setTitle(Const.mostPopularTitle)
        return sectionView
    }()
    
    private(set) lazy var popularMoviesTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(MovieTableViewCell.self, forCellReuseIdentifier: MovieTableViewCell.identifier)
        tableView.delegate = viewModel.tableDataSource
        tableView.dataSource = viewModel.tableDataSource
        tableView.estimatedRowHeight = 100.0
        tableView.backgroundColor = Const.backgroundColor
        return tableView
    }()
    
    private let viewModel: MainScreenViewModel
    
    init(viewModel: MainScreenViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        setupPlayingNowHeaderConstraints()
        setupPlayingNowCollectionConstraints()
        setupPopularHeaderConstraints()
        setupPopularMoviesConstraints()
        viewModel.fetchData()
    }
}

extension MainScreenViewController {
    private func setupLayout() {
        title = Const.viewTitle
        view.addSubview(playingNowHeaderView)
        view.addSubview(playingNowMoviewCollectionView)
        view.addSubview(popularHeaderView)
        view.addSubview(popularMoviesTableView)
    }
    
    private func setupPlayingNowHeaderConstraints() {
        playingNowHeaderView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalToSuperview()
        }
    }
    
    private func setupPlayingNowCollectionConstraints() {
        playingNowMoviewCollectionView.snp.makeConstraints { make in
            make.top.equalTo(playingNowHeaderView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(160.0)
        }
    }
    
    private func setupPopularHeaderConstraints() {
        popularHeaderView.snp.makeConstraints { make in
            make.top.equalTo(playingNowMoviewCollectionView.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
    }
    
    private func setupPopularMoviesConstraints() {
        popularMoviesTableView.snp.makeConstraints { make in
            make.top.equalTo(popularHeaderView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.snp.bottom)
        }
    }
}

extension MainScreenViewController: MainScreenViewDelegate {
    func presentAlertController(_ viewController: UIViewController) {
        present(viewController, animated: true, completion: nil)
    }
    
    func presentDetailsView(_ viewController: UIViewController) {
        viewController.modalPresentationStyle = .overFullScreen
        present(viewController, animated: true, completion: nil)
    }
    
    func reloadTableView() {
        popularMoviesTableView.reloadData()
    }
    
    func reloadCollectionView() {
        playingNowMoviewCollectionView.reloadData()
    }
}

