//
//  MainViewModel.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit
import Combine
import DAL
import Networking

protocol MainScreenViewDelegate: AnyObject {
    func presentAlertController(_ viewController: UIViewController)
    func presentDetailsView(_ viewController: UIViewController)
    func reloadCollectionView()
    func reloadTableView()
}

final class MainScreenViewModel {
    
    enum Const {
        static let alertTitle = "Alert"
        static let okTitle = "Ok"
        static let tryAgain = "Try again"
        static let noInternetConnectionMessage = "There is no internet connection!"
    }
    
    weak var delegate: MainScreenViewDelegate?
    
    private let dal: DAL
    private let imageRepository: ImageRepositoryProtocol
    private var disposables = Set<AnyCancellable>()
    
    private(set) var tableDataSource = MainScreenTableDataManager()
    private(set) var collectionDataSource = MainScreenCollectionDataManager()
    
    private var pageNumber = 0
    private var pageCount = 0
    
    init(dal: DAL, imageRepository: ImageRepositoryProtocol) {
        self.dal = dal
        self.imageRepository = imageRepository
        self.collectionDataSource.imageRepository = imageRepository
        self.tableDataSource.imageRepository = imageRepository
        setUpClosures()
    }
}

extension MainScreenViewModel {
    func fetchData() {
        fetchPopularMovies()
        fetchPlayingNowMovies()
    }
    
    private func fetchPopularMovies() {
        guard pageNumber <= pageCount else { return }
        pageNumber += 1
        dal.executeRequest(PopularMoviesRequest(page: pageNumber))?
            .receive(on: DispatchQueue.main)
            .sink { [weak self] response in
                if case let .failure(error) = response {
                    guard let self = self else { return }
                    self.delegate?.presentAlertController(self.prepareAlertController(with: error, tryAgainCompletion: { [weak self] in
                        self?.fetchPopularMovies()
                    }))
                }
            } receiveValue: { [weak self] result in
                self?.pageCount = result.totalPages
                self?.tableDataSource.items.append(contentsOf: result.results.map({ MovieCellViewModel(movie: $0, imageRepository: self?.imageRepository, dal: self?.dal)}))
                self?.delegate?.reloadTableView()
            }.store(in: &disposables)
    }
    
    private func fetchPlayingNowMovies() {
        dal.executeRequest(PlayingNowRequest())?
            .receive(on: DispatchQueue.main)
            .sink { [weak self] response in
                if case let .failure(error) = response {
                    guard let self = self else { return }
                    self.delegate?.presentAlertController(self.prepareAlertController(with: error, tryAgainCompletion: { [weak self] in
                        self?.fetchPlayingNowMovies()
                    }))
                }
            } receiveValue: { [weak self] result in
                self?.collectionDataSource.items = result.results.map({ MovieCollectionCellModel(movie: $0, imageRepository: self?.imageRepository)})
                self?.delegate?.reloadCollectionView()
            }.store(in: &disposables)
    }
    
    private func setUpClosures() {
        tableDataSource.didSelectItemAction = { [weak self] movie in
            self?.prepareDetailsViewControllerWith(movie)
        }
        
        tableDataSource.lastItemWasReached = { [weak self] in
            self?.fetchPopularMovies()
        }
        
        collectionDataSource.didSelectItemAction = { [weak self] movie in
            self?.prepareDetailsViewControllerWith(movie)
        }
    }
    
    private func prepareDetailsViewControllerWith(_ movie: Movie) {
        let detailsViewModel = DetailsScreenViewModel(movie: movie)
        let detailsViewController = DetailsScreenViewController(viewModel: detailsViewModel)
        delegate?.presentDetailsView(detailsViewController)
    }
    
    private func prepareAlertController(with error: Error, tryAgainCompletion: @escaping () -> ()) -> UIAlertController {
        let alertController = UIAlertController(title: Const.alertTitle, message: Const.noInternetConnectionMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: Const.okTitle, style: .cancel)
        let tryAgainAction = UIAlertAction(title: Const.tryAgain, style: .default) { _ in
            tryAgainCompletion()
        }
        
        alertController.addAction(okAction)
        alertController.addAction(tryAgainAction)
        return alertController
    }
}
