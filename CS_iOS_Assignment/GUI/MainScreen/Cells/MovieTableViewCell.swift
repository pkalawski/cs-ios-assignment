//
//  MovieTableViewCell.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit
import SnapKit
import DAL

final class MovieTableViewCell: UITableViewCell {
    
    enum Const {
        static let labelTextColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0)
        static let titleFont = UIFont(name: "HelveticaNeue-Bold", size: 14.0)
        static let contentFont = UIFont(name: "HelveticaNeue", size: 12.0)
        
        enum Separator {
            static let backgroundColor = UIColor(red: 64/255, green: 64/255, blue: 64/255, alpha: 1.0)
        }
        
        enum MovieImage {
            static let borderWidth: CGFloat = 1.0
            static let borderColor = UIColor(red: 130/255, green: 130/255, blue: 130/255, alpha: 1.0).cgColor
            static let imageSize = CGSize(width: 50.0, height: 80.0)
        }
        
        enum ContentView {
            static let backgroundColor = UIColor(red: 33/255, green: 33/255, blue: 33/255, alpha: 1.0)
        }
    }
    
    private(set) lazy var movieImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.borderWidth = Const.MovieImage.borderWidth
        imageView.layer.borderColor = Const.MovieImage.borderColor
        return imageView
    }()
    
    private lazy var labelsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, releaseDateLabel, durationLabel])
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        var label = UILabel()
        label.font = Const.titleFont
        label.textColor = Const.labelTextColor
        return label
    }()
    
    private lazy var releaseDateLabel: UILabel = {
        var label = UILabel()
        label.font = Const.contentFont
        label.textColor = Const.labelTextColor
        return label
    }()
    
    private lazy var durationLabel: UILabel = {
        var label = UILabel()
        label.font = Const.contentFont
        label.textColor = Const.labelTextColor
        return label
    }()
    
    private lazy var ratingView: RatingView = {
        let ratingView = RatingView()
        return ratingView
    }()
    
    private lazy var separatorLine: UIView = {
        var view = UIView()
        view.backgroundColor = Const.Separator.backgroundColor
        return view
    }()
    
    var viewModel: MovieCellModelProtocol? {
        didSet {
            oldValue?.parentCell = nil
            viewModel?.parentCell = self
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout() {
        contentView.backgroundColor = Const.ContentView.backgroundColor
        selectionStyle = .none
        contentView.addSubview(labelsStackView)
        contentView.addSubview(movieImage)
        contentView.addSubview(ratingView)
        contentView.addSubview(separatorLine)
    }
    
    func setupConstraints() {
        setupComponentsConstraints()
    }
    
    func setupComponentsConstraints() {
        movieImage.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8.0)
            make.leading.equalToSuperview().offset(24.0)
            make.size.equalTo(Const.MovieImage.imageSize)
        }
        
        labelsStackView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(16.0)
            make.leading.equalTo(movieImage.snp.trailing).offset(18.0)
            make.bottom.lessThanOrEqualTo(movieImage).priority(.high)
        }
        
        ratingView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(labelsStackView.snp.trailing)
            make.trailing.equalToSuperview().offset(-25.0)
            make.size.equalTo(38.0)
        }
        
        separatorLine.snp.makeConstraints { make in
            make.top.equalTo(movieImage.snp.bottom).offset(8.0)
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(4.0).priority(.high)
        }
        
        labelsStackView.setCustomSpacing(8.0, after: titleLabel)
        labelsStackView.setCustomSpacing(2.0, after: releaseDateLabel)
    }
    
    func configure(withModel movie: Movie) {
        titleLabel.text = movie.title
        releaseDateLabel.text = DateConverter().convertDate(from: movie.releaseDate)

        ratingView.isHidden = movie.voteAverage == 0.0
        ratingView.progress = CGFloat(movie.voteAverage / 10)
    }
    
    func setImage(_ image: UIImage?) {
        movieImage.image = image
    }
    
    func setRuntime(with runtime: String) {
        durationLabel.text = runtime
    }
}
