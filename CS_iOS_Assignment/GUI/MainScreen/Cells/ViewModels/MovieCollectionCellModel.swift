//
//  MovieCollectionCellModel.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import UIKit
import DAL

protocol MovieCollectionCellModelProtocol: AnyObject {
    var parentCell: MovieCollectionViewCell? { get set }
}

final class MovieCollectionCellModel: MovieCollectionCellModelProtocol {
    weak var parentCell: MovieCollectionViewCell? {
        didSet {
            updateCell()
            fetchImage()
        }
    }
    
    private(set) var movie: Movie
    private let imageRepository: ImageRepositoryProtocol?
    
    private(set) var movieImage: UIImage?
    
    init(movie: Movie, imageRepository: ImageRepositoryProtocol?) {
        self.movie = movie
        self.imageRepository = imageRepository
    }
}

extension MovieCollectionCellModel {
    private func updateCell() {
        guard let cell = self.parentCell else {
            return
        }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            cell.setImage(self.movieImage)
        }
    }
    
    private func fetchImage() {
        guard (parentCell != nil) else { return }
        guard (movieImage == nil) else { return }
        
        guard let posterPath = movie.posterPath else { return }
        imageRepository?.getImageFrom(url: "\(URLConst.imageUrlStringBaseURL)\(posterPath)", completion: { [weak self] result in
            switch result {
            case let .success(image):
                self?.setImageAndRefreshCell(image: image)
            case .failure(ImageRepositoryError.failedURL):
                self?.setImageAndRefreshCell(image: UIImage(named: "placeholder"))
            case let .failure(error):
                print(error)
            }
        })
    }
    
    private func setImageAndRefreshCell(image: UIImage?) {
        movieImage = image
        updateCell()
    }
}
