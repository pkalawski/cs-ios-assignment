//
//  MovieCellViewModel.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import UIKit
import Combine
import DAL
import Networking

protocol MovieCellModelProtocol: AnyObject {
    var parentCell: MovieTableViewCell? { get set }
}

final class MovieCellViewModel: MovieCellModelProtocol {
    weak var parentCell: MovieTableViewCell? {
        didSet {
            updateCell()
            fetchAdditionalData()
        }
    }

    private let imageRepository: ImageRepositoryProtocol?
    private let dal: DAL?
    
    private var disposables = Set<AnyCancellable>()
    
    private(set) var movie: Movie
    private(set) var movieImage: UIImage?
    private(set) var runtime: Time?
    
    init(movie: Movie, imageRepository: ImageRepositoryProtocol?, dal: DAL?) {
        self.movie = movie
        self.imageRepository = imageRepository
        self.dal = dal
    }
}

extension MovieCellViewModel {
    private func updateCell() {
        guard let cell = self.parentCell else {
            return
        }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self, let runtime = self.runtime else { return }
            cell.configure(withModel: self.movie)
            cell.setImage(self.movieImage)
            cell.setRuntime(with: "\(runtime.hours)h \(runtime.minutes)m")
        }
    }
    
    private func fetchAdditionalData() {
        guard (parentCell != nil) else { return }
        guard (movieImage == nil) else { return }
        guard (runtime == nil) else { return }
        
        guard let posterPath = movie.posterPath else { return }
        imageRepository?.getImageFrom(url: "\(URLConst.imageUrlStringBaseURL)\(posterPath)", completion: { [weak self] result in
            switch result {
            case let .success(image):
                self?.setImageAndRefreshCell(image: image)
            case .failure(ImageRepositoryError.failedURL):
                self?.setImageAndRefreshCell(image: UIImage(named: "placeholder"))
            case let .failure(error):
                print(error)
            }
        })
        
        dal?.executeRequest(MovieDetailsRequest(movieId: movie.id))?
            .sink(receiveCompletion: { _ in }, receiveValue: { [weak self] movieDetails in
                self?.setRuntimeAndRefreshCell(time: movieDetails.runtime)
            }).store(in: &disposables)
    }
    
    private func setRuntimeAndRefreshCell(time: Int) {
        runtime = TimeConverter().toHours(runtime: time)
        DispatchQueue.main.async { [weak self] in
            guard let self = self, let runtime = self.runtime else { return }
            self.parentCell?.setRuntime(with: "\(runtime.hours)h \(runtime.minutes)m")
        }
    }
    
    private func setImageAndRefreshCell(image: UIImage?) {
        movieImage = image
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.parentCell?.setImage(image)
        }
    }
}
