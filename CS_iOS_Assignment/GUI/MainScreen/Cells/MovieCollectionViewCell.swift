//
//  MovieCollectionViewCell.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class MovieCollectionViewCell: UICollectionViewCell {
    
    enum Const {
        static let imageSize = CGSize(width: 100.0, height: 160.0)
    }
    
    private(set) lazy var movieImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    var viewModel: MovieCollectionCellModel? {
        didSet {
            oldValue?.parentCell = nil
            viewModel?.parentCell = self
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MovieCollectionViewCell {
    func setupLayout() {
        contentView.addSubview(movieImage)
    }
    
    func setupConstraints() {
        setupComponentsConstraints()
    }
    
    func setupComponentsConstraints() {
        movieImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.size.equalTo(Const.imageSize)
        }
    }
    
    func setImage(_ image: UIImage?) {
        movieImage.image = image
    }
}
