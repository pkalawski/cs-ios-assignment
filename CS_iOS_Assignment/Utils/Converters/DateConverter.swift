//
//  DateConverter.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

final class DateConverter {
    
    func convertDate(from dateString: String?) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let dateString = dateString, let date = dateFormatter.date(from: dateString) {
            dateFormatter.dateFormat = "MMMM d, yyyy"
            return dateFormatter.string(from: date)
        }
        
        return nil
    }
}
