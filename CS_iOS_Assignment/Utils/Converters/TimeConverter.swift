//
//  TimeConverter.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

final class TimeConverter {
    
    func toHours(runtime: Int) -> Time {
        let hours = Int(runtime/60)
        let minutes = runtime - hours * 60
        return Time(hours: hours, minutes: minutes)
    }
}

struct Time {
    let hours: Int
    let minutes: Int
}
