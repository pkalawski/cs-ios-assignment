//
//  RequestHandler+Extensions.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import DAL
import Networking

extension DAL {
    static let shared = DAL(networking: Networking(requestHandler: RequestHandler(basePath: "https://api.themoviedb.org/3/movie/", apiKey: "55957fcf3ba81b137f8fc01ac5a31fb5")))
}
