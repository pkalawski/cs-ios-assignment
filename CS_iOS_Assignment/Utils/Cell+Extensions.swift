//
//  Cell+Extensions.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

extension UITableView {
    func dequeueReusableCell<CellType: UITableViewCell>(for indexPath: IndexPath) -> CellType? {
        dequeueReusableCell(withIdentifier: "\(CellType.self)", for: indexPath) as? CellType
    }
}

extension UICollectionView {
    func dequeueReusableCell<CellType: UICollectionViewCell>(for indexPath: IndexPath) -> CellType? {
        dequeueReusableCell(withReuseIdentifier: "\(CellType.self)", for: indexPath) as? CellType
    }
}

extension NSObject {
    class var identifier: String { return String(describing: self) }
}
