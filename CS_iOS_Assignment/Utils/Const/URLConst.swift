//
//  URLConst.swift
//  CS_iOS_Assignment
//
//  Created by Przemysław Kalawski on 02/01/2021.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

class URLConst {
    static let imageUrlStringBaseURL = "https://image.tmdb.org/t/p/w500"
}
