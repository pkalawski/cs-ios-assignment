import Foundation
import Combine

public class Networking {
    
    let requestHandler: RequestHandler?
    
    public init(requestHandler: RequestHandler? = nil) {
        self.requestHandler = requestHandler
    }
    
    public func executeRequest<DTO>(_ request: HttpRequest<DTO>) -> AnyPublisher<DTO, NetworkingError>? {
        requestHandler?.executeRequest(request)
    }
    
}

