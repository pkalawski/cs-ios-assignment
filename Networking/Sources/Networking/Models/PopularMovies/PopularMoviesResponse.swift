//  PopularMoviesResponse.swift
//  Created by Przemysław Kalawski on 02/01/2021.

import Foundation

public struct PopularMoviesResponse: Decodable {
    public let page: Int
    public let results: [MovieDTO]
    public let totalPages: Int
    public let totalResults: Int
    
    public init(page: Int, results: [MovieDTO], totalPages: Int, totalResults: Int) {
        self.page = page
        self.results = results
        self.totalPages = totalPages
        self.totalResults = totalResults
    }
}
