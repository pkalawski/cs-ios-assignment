//  MovieDetailsDTO.swift
//  Created by Przemysław Kalawski on 02/01/2021.


import Foundation

public enum NetworkingModel { }

extension NetworkingModel {
    // MARK: - MovieDetailsDTO
    public struct MovieDetailsDTO: Decodable {
        public let adult: Bool
        public let backdropPath: String?
        public let budget: Int
        public let genres: [Genre]
        public let homepage: String
        public let id: Int
        public let imdbId: String
        public let originalLanguage: String
        public let originalTitle: String
        public let overview: String
        public let popularity: Double
        public let posterPath: String
        public let productionCompanies: [ProductionCompany]
        public let productionCountries: [ProductionCountry]
        public let releaseDate: String
        public let revenue: Int
        public let runtime: Int
        public let spokenLanguages: [SpokenLanguage]
        public let status: String
        public let tagline: String
        public let title: String
        public let video: Bool
        public let voteAverage: Double
        public let voteCount: Int
        
        public init(adult: Bool, backdropPath: String?, budget: Int, genres: [NetworkingModel.Genre], homepage: String, id: Int, imdbId: String, originalLanguage: String, originalTitle: String, overview: String, popularity: Double, posterPath: String, productionCompanies: [NetworkingModel.ProductionCompany], productionCountries: [NetworkingModel.ProductionCountry], releaseDate: String, revenue: Int, runtime: Int, spokenLanguages: [NetworkingModel.SpokenLanguage], status: String, tagline: String, title: String, video: Bool, voteAverage: Double, voteCount: Int) {
            self.adult = adult
            self.backdropPath = backdropPath
            self.budget = budget
            self.genres = genres
            self.homepage = homepage
            self.id = id
            self.imdbId = imdbId
            self.originalLanguage = originalLanguage
            self.originalTitle = originalTitle
            self.overview = overview
            self.popularity = popularity
            self.posterPath = posterPath
            self.productionCompanies = productionCompanies
            self.productionCountries = productionCountries
            self.releaseDate = releaseDate
            self.revenue = revenue
            self.runtime = runtime
            self.spokenLanguages = spokenLanguages
            self.status = status
            self.tagline = tagline
            self.title = title
            self.video = video
            self.voteAverage = voteAverage
            self.voteCount = voteCount
        }
    }

    // MARK: - Genre
    public struct Genre: Decodable {
        public let id: Int
        public let name: String
    }

    // MARK: - ProductionCompany
    public struct ProductionCompany: Decodable {
        public let id: Int
        public let logoPath: String?
        public let name: String
        public let originCountry: String
    }

    // MARK: - ProductionCountry
    public struct ProductionCountry: Decodable {
        public let name: String
    }

    // MARK: - SpokenLanguage
    public struct SpokenLanguage: Decodable {
        public let englishName: String
        public let name: String
    }
}
