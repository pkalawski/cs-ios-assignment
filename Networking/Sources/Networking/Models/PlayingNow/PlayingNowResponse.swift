//  PlayingNowResponse.swift
//  Created by Przemysław Kalawski on 02/01/2021.


import Foundation

public struct PlayingNowResponse: Decodable {
    public let dates: Dates
    public let page: Int
    public let results: [MovieDTO]
    public let totalPages: Int
    public let totalResults: Int
    
    public init(dates: Dates, page: Int, results: [MovieDTO], totalPages: Int, totalResults: Int) {
        self.dates = dates
        self.page = page
        self.results = results
        self.totalPages = totalPages
        self.totalResults = totalResults
    }
}
