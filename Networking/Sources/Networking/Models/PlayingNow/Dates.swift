//  Dates.swift
//  Created by Przemysław Kalawski on 02/01/2021.

import Foundation

public struct Dates: Decodable {
    public let maximum: String
    public let minimum: String
    
    public init(maximum: String, minimum: String) {
        self.maximum = maximum
        self.minimum = minimum
    }
}
