//  PopularMoviesRequest.swift
//  Created by Przemysław Kalawski on 02/01/2021.

import Foundation

public final class PopularMoviesRequest: HttpRequest<PopularMoviesResponse> {
    
    public override var path: String { "popular"}
    private let page: Int
    
    public init(page: Int) {
        self.page = page
        super.init()
        queryItems.append(URLQueryItem(name: "page", value: "\(page)"))
    }
}
