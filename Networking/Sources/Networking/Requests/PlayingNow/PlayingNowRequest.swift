//  PlayingNowRequest.swift
//  Created by Przemysław Kalawski on 02/01/2021.

import Foundation

public final class PlayingNowRequest: HttpRequest<PlayingNowResponse> {
    
    public override var path: String { "now_playing" }
    
    public override init() {} 
}
