//  MovieDetailsRequest.swift
//  Created by Przemysław Kalawski on 02/01/2021.

import Foundation

public final class MovieDetailsRequest: HttpRequest<NetworkingModel.MovieDetailsDTO> {
    
    public override var path: String { "\(movieId)"}
    
    private let movieId: Int
    
    public init(movieId: Int) {
        self.movieId = movieId
    }
}
