//  HttpRequest.swift
//  Created by Przemysław Kalawski on 02/01/2021.

import Foundation

public class HttpRequest<DTO: Decodable> {
    
    private lazy var jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    public var path: String { "" }
    public var body: Data?
    public var queryItems: [URLQueryItem] = []
    public var headers: [String: String] = [:]
    public var params: [String: String] = [:]
    public var method: HttpMethod { .get }
    
    public func getUrlString(baseUrlString: String) -> String {
        baseUrlString + path
    }
    
    public func parseResponse(data: Data, response: URLResponse) throws -> DTO {
        guard response is HTTPURLResponse else {
            throw NetworkingError.invalidResponse(response: response)
        }
        
        return try jsonDecoder.decode(DTO.self, from: data)
    }
}

public enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
