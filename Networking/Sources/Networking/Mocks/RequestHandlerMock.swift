//  RequestHandlerMock.swift
//  Created by Przemysław Kalawski on 03/01/2021.

import Foundation
import Combine

public final class RequestHandlerMock<DTO, Request: HttpRequest<DTO>> {
    
    private let networkingMock: NetworkingMock
    private let requestType: Request.Type
    private var condition: (Request) -> Bool = { _ in true }
    private var handler: ((Request) -> Result<DTO, NetworkingError>)?
    
    public init(networkingMock: NetworkingMock, requestType: Request.Type) {
        self.networkingMock = networkingMock
        self.requestType = requestType
    }
    
    public func condition(_ condition: @escaping (Request) -> Bool) -> Self {
        self.condition = condition
        return self
    }
    
    @discardableResult
    public func then(_ handler: @escaping (Request) -> Result<DTO, NetworkingError>) -> RequestHandlerMock {
        networkingMock.registerMock(self)
        self.handler = handler
        return self
    }
}
extension RequestHandlerMock: RequestHandlerProtocol {
    public func executeRequest<DTO>(_ request: HttpRequest<DTO>) -> AnyPublisher<DTO, NetworkingError>? where DTO : Decodable {
        guard type(of: request).self == requestType,
              let typedRequest = request as? Request,
              condition(typedRequest),
              let handler = handler else { return nil }
        
        guard let result = handler(typedRequest) as? Result<DTO, NetworkingError> else { return nil }
        
        return result.publisher.eraseToAnyPublisher()
    }
}
