//  NetworkingMock.swift
//  Created by Przemysław Kalawski on 03/01/2021.

import Foundation
import Combine

public class NetworkingMock: Networking {
    
    private var handlerMocks: [RequestHandlerProtocol] = []
    
    public override func executeRequest<DTO>(_ request: HttpRequest<DTO>) -> AnyPublisher<DTO, NetworkingError>? where DTO : Decodable {
        for handlerMock in handlerMocks {
            if let result = handlerMock.executeRequest(request) {
                return result
            }
        }
        
        return Empty().eraseToAnyPublisher()
    }
}

extension NetworkingMock {
    public func when<DTO, Request: HttpRequest<DTO>>(_ requestType: Request.Type) -> RequestHandlerMock<DTO, Request> {
        RequestHandlerMock<DTO, Request>(networkingMock: self, requestType: requestType)
    }
    
    public func registerMock<DTO, Request: HttpRequest<DTO>>(_ handlerMock: RequestHandlerMock<DTO, Request>) {
        handlerMocks.append(handlerMock)
    }
}
