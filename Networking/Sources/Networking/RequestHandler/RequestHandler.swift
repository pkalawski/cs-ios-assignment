import Foundation
import Combine

public protocol RequestHandlerProtocol {
    func executeRequest<DTO>(_ request: HttpRequest<DTO>) -> AnyPublisher<DTO, NetworkingError>? where DTO: Decodable
}

public final class RequestHandler {
    private let basePath: String
    private let apiKey: String
    private let language: String?
    private let urlSession: URLSession
    
    public init(basePath: String, apiKey: String, language: String? = Locale.current.languageCode, urlSession: URLSession = .shared) {
        self.basePath = basePath
        self.apiKey = apiKey
        self.language = language
        self.urlSession = urlSession
    }
}

extension RequestHandler: RequestHandlerProtocol {
    public func executeRequest<DTO>(_ request: HttpRequest<DTO>) -> AnyPublisher<DTO, NetworkingError>? where DTO: Decodable {
        let urlRequest = createRequest(request: request)
        
        switch urlRequest {
        case let .failure(error):
            return Fail(error: error).eraseToAnyPublisher()
        case let .success(urlRequest):
            return urlSession.dataTaskPublisher(for: urlRequest)
                .tryMap { data, response  in
                    do {
                        return try request.parseResponse(data: data, response: response)
                    } catch let error as DecodingError {
                        throw NetworkingError.parsingFailed(error: error, data: data)
                    }
                }
                .mapError(NetworkingError.from(error:))
                .eraseToAnyPublisher()
        }
    }
}

extension RequestHandler {
    private func createRequest<DTO>(request: HttpRequest<DTO>) -> Result<URLRequest, NetworkingError> {
        let urlString = request.getUrlString(baseUrlString: basePath)
        guard var urlComponents = URLComponents(string: urlString) else {
            return .failure(NetworkingError.invalidUrl(url: urlString))
        }
        urlComponents.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "language", value: language)
        ]
        urlComponents.queryItems?.append(contentsOf: request.queryItems)
        
        guard let url = urlComponents.url else {
            return .failure(NetworkingError.invalidUrl(url: urlString))
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.httpBody = request.body
        urlRequest.allHTTPHeaderFields = request.headers
        
        return .success(urlRequest)
    }
}

public enum NetworkingError: Error {
    case invalidUrl(url: String)
    case invalidResponse(response: URLResponse)
    case parsingFailed(error: Error, data: Data)
    case unknown(error: Error)
    
    public static func from(error: Error) -> NetworkingError {
        if let error = error as? NetworkingError {
            return error
        } else {
            return .unknown(error: error)
        }
    }
}
