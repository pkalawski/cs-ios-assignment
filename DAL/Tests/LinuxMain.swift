import XCTest

import DALTests

var tests = [XCTestCaseEntry]()
tests += DALTests.allTests()
XCTMain(tests)
