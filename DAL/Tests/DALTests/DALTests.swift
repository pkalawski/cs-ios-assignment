import XCTest
import Combine
import Networking

@testable import DAL

final class DALTests: XCTestCase {
    func testExample() {
        let sut = DAL(networking: Networking(requestHandler: RequestHandler(basePath: "https://api.themoviedb.org/3/movie/", apiKey: "55957fcf3ba81b137f8fc01ac5a31fb5")))
        var disposables = Set<AnyCancellable>()
        
        func testFetchingPopularMovies() {
            let exp = expectation(description: "test")
            let request = PopularMoviesRequest(page: 1)
            sut.executeRequest(request)?
                .sink { result in
                    print(result)
                } receiveValue: { items in
                    print(items)
                    exp.fulfill()
                }.store(in: &disposables)

            wait(for: [exp], timeout: 1.0)
        }
        
        func testPlayingNowMovies() {
            let exp = expectation(description: "test")
            let request = PlayingNowRequest()
            sut.executeRequest(request)?
                .sink { result in
                    print(result)
                } receiveValue: { items in
                    print(items)
                    exp.fulfill()
                }.store(in: &disposables)

            wait(for: [exp], timeout: 1.0)
        }
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
