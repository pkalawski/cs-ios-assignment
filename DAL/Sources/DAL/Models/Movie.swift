//  Movie.swift
//  Created by Przemysław Kalawski on 03/01/2021.

import Foundation
import Networking

public struct Movie: Decodable {
    public let adult: Bool
    public let backdropPath: String?
    public let genreIds: [Int]
    public let id: Int
    public let originalLanguage: String
    public let originalTitle: String
    public let overview: String
    public let popularity: Double
    public let posterPath: String?
    public let releaseDate: String?
    public let title: String
    public let video: Bool
    public let voteAverage: Double
    public let voteCount: Int
}

extension Movie: MappableFromDTO {
    public init(_ dto: MovieDTO) {
        self.adult = dto.adult
        self.backdropPath = dto.backdropPath
        self.genreIds = dto.genreIds
        self.id = dto.id
        self.originalLanguage = dto.originalLanguage
        self.originalTitle = dto.originalTitle
        self.overview = dto.overview
        self.popularity = dto.popularity
        self.posterPath = dto.posterPath
        self.releaseDate = dto.releaseDate
        self.title = dto.title
        self.video = dto.video
        self.voteAverage = dto.voteAverage
        self.voteCount = dto.voteCount
    }
}
