//  PopularMovies.swift
//  Created by Przemysław Kalawski on 03/01/2021.

import Foundation
import Networking

public struct PopularMovies: Decodable {
    public let page: Int
    public let results: [Movie]
    public let totalPages: Int
    public let totalResults: Int
}

extension PopularMovies: MappableFromDTO {
    public init(_ dto: PopularMoviesResponse) {
        self.page = dto.page
        self.results = dto.results.map { Movie($0) }
        self.totalPages = dto.totalPages
        self.totalResults = dto.totalResults
    }
}
