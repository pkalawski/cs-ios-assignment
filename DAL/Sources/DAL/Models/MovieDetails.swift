//  MovieDetails.swift
//  Created by Przemysław Kalawski on 03/01/2021.

import Foundation
import Networking

// MARK: - MovieDetails
public struct MovieDetails: Decodable {
    public let adult: Bool
    public let backdropPath: String?
    public let budget: Int
    public let genres: [Genre]
    public let homepage: String
    public let id: Int
    public let imdbId: String
    public let originalLanguage: String
    public let originalTitle: String
    public let overview: String
    public let popularity: Double
    public let posterPath: String
    public let releaseDate: String
    public let revenue: Int
    public let runtime: Int
    public let status: String
    public let tagline: String
    public let title: String
    public let video: Bool
    public let voteAverage: Double
    public let voteCount: Int
}

extension MovieDetails: MappableFromDTO {
    public init(_ dto: NetworkingModel.MovieDetailsDTO) {
        self.adult = dto.adult
        self.backdropPath = dto.backdropPath
        self.budget = dto.budget
        self.genres = dto.genres.map { Genre($0)}
        self.homepage = dto.homepage
        self.id = dto.id
        self.imdbId = dto.imdbId
        self.originalLanguage = dto.originalLanguage
        self.originalTitle = dto.originalTitle
        self.overview = dto.overview
        self.popularity = dto.popularity
        self.posterPath = dto.posterPath
        self.releaseDate = dto.releaseDate
        self.revenue = dto.revenue
        self.runtime = dto.runtime
        self.status = dto.status
        self.tagline = dto.tagline
        self.title = dto.title
        self.video = dto.video
        self.voteAverage = dto.voteAverage
        self.voteCount = dto.voteCount
    }
}
