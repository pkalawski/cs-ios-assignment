//  RequestMapping.swift
//  Created by Przemysław Kalawski on 03/01/2021.

import Foundation
import Networking

extension PopularMoviesRequest: ModelMappable {
    public typealias Model = PopularMovies
}

extension PlayingNowRequest: ModelMappable {
    public typealias Model = PlayingNowMovies
}

extension MovieDetailsRequest: ModelMappable {
    public typealias Model = MovieDetails
}
