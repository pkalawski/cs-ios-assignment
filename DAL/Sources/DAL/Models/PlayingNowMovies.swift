//  PlayingNowMovies.swift
//  Created by Przemysław Kalawski on 03/01/2021.

import Foundation
import Networking

public struct PlayingNowMovies: Decodable {
    public let results: [Movie]
    public let totalPages: Int
}

extension PlayingNowMovies: MappableFromDTO {
    public init(_ dto: PlayingNowResponse) {
        self.results = dto.results.map { Movie ($0)}
        self.totalPages = dto.totalPages
    }
}
