//  Genre.swift
//  Created by Przemysław Kalawski on 03/01/2021.

import Foundation
import Networking

// MARK: - Genre
public struct Genre: Decodable {
    public let id: Int
    public let name: String
}

extension Genre: MappableFromDTO {
    public init(_ dto: NetworkingModel.Genre) {
        self.id = dto.id
        self.name = dto.name
    }
}
