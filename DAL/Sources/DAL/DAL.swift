//  DAL.swift
//  Created by Przemysław Kalawski on 03/01/2021.

import Foundation
import Networking
import Combine

public class DAL {
    private let networking: Networking
    
    public init(networking: Networking) {
        self.networking = networking
    }
    
    public func executeRequest<DTO, Request: HttpRequest<DTO>, Model>(_ request: Request) -> AnyPublisher<Model, NetworkingError>?
    where Request: ModelMappable, Request.Model == Model, Model.DTO == DTO {
        networking.executeRequest(request)?
            .map { Model($0)}
            .eraseToAnyPublisher()
    }
}

public protocol MappableFromDTO {
    associatedtype DTO
    init(_ dto: DTO)
}

public protocol ModelMappable {
    associatedtype Model: MappableFromDTO
}
